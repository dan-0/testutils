A library for simplifying some aspects of testing.

# Blocking Test

`blockingTest` wraps the `runBlockingTest` coroutine allowing for setup and cleanup initialization
parameters to be specified. This allows for setting up and tearing down test implementations within
the same `TestCoroutineScope`


```kotlin
internal class HomeStateHandlerTest {

    private lateinit var ut: HomeStateHandler
    private lateinit var testChannel: Channel<String>

    private fun setup(scope: CoroutineScope) {

        fakeHelper = FakeInteractor()

        testChannel = Channel()
        
        ut = ClassUnderTest()
    }

    private fun cleanup() {
        testChannel.cancel()
    }

    @Test
    fun `temp fun`() = blockingTest(::setup, ::cleanup) {
        val testString = "My String"

        var lastReceivedString: String? = null
        launch {
            testChannel.consumeEach {
                lastReceivedString = it
            }
        }

        testChannel.send(testString)

        assertEquals(testString, lastReceivedString)
    }
}
```