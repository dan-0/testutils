package me.danlowe.testutils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import kotlin.system.measureTimeMillis

class TestExtensionsKtTest {

    @Test
    fun `default behavior`() {
        val oneSecond = 1000L
        val numberOfRuns = 10

        // 10 second blocking run length
        val blockingTestLength = 10 * oneSecond

        var runLength: Long = -1
        val addedNumbers = mutableListOf<Int>()
        val range = (1..numberOfRuns)
        blockingTest {
            runLength = measureTimeMillis {
                range.forEach {
                    delay(oneSecond)
                    addedNumbers.add(it)
                }
            }
        }

        assertTrue(
            runLength < blockingTestLength,
            "Expected run length to be less than actual blocked seconds " +
                    "$blockingTestLength. Instead was $runLength"
        )
        assertEquals(
            numberOfRuns,
            addedNumbers.size
        )
    }

    @Test
    fun `setup called`() {
        var setupCalled = false
        val setup = fun(_: CoroutineScope) {
            setupCalled = true
        }

        blockingTest(setup) {}

        assertTrue(setupCalled)
    }

    @Test
    fun `cleanup called`() {
        var cleanupCalled = false
        val cleanup = fun() {
            cleanupCalled = true
        }

        blockingTest(cleanup = cleanup) {}

        assertTrue(cleanupCalled)
        fail<String>("asdf")
    }
}