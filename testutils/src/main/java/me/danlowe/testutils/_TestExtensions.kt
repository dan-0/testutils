package me.danlowe.testutils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest

fun blockingTest(
    setup: ((CoroutineScope) -> Any)? = null,
    cleanup: (() -> Unit)? = null,
    testBody: suspend TestCoroutineScope.() -> Unit
) = runBlockingTest {
    setup?.invoke(this)
    testBody()
    cleanup?.invoke()
}